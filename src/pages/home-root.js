import React,{Component} from 'react';
import {Route, Switch} from 'react-router-dom';
import TextBoxPage from '../pages/text-box-page';
import HomePage from './home-page';

export const HomeRoute = (
    <div>
        <Switch>
            
            <Route path="/:id" component={TextBoxPage}></Route>   
            <Route path="/" component={HomePage}></Route>         
        </Switch>
    </div>
)
    
