import React,{Component} from 'react';
import firebase from 'firebase/app';
import 'firebase/database';
import {fireBaseConfig} from '../config/firebase-config';
import ReactQuill from 'react-quill';
import 'react-quill/dist/quill.snow.css';
import { object } from 'prop-types';

let _fire = null;
export default class TextBox extends Component{


    constructor(props){
        super(props);
        this.state = {id :"",userId:localStorage.getItem("id"),text:"",readOnly:false,control:localStorage.getItem("id")}
        _fire = firebase.initializeApp(fireBaseConfig);


        _fire.database().ref("/sample/"+this.props.match.params.id).on('child_changed', (child) => {
            let message = child.val();
            if(message.userId === null || !message.userId || message.userId == this.state.userId){
                this.setState({readOnly:false,text:message.value,control:message.userId});
            }
            else{
                this.setState({readOnly:true,text:message.value,control:message.userId});
            }
            // document.getElementById("document").value = message.value;
		});
    }

    componentDidMount(){
        this.setState({id:this.props.match.params.id});
        // let connectedRef = _fire.database().ref(".info/connected");
        // connectedRef.on("value", function (snap) {
        //     if (snap.val() === true) {

        //     } else {
                
        //     }
        // });
        // document.getElementById("document").readOnly = false;
        _fire.database().ref("/sample/"+this.props.match.params.id).on('child_added', (child) => {
            let message = child.val();
            if(message != null && message.userId != null && message.userId != this.state.userId){
                this.setState({text:message.value,readOnly:true,control:message.userId});
            }
            else{
                this.setState({text:message.value,readOnly:false,control:message.userId});
            }
            //  document.getElementById("document").value = message.value;
        });
    }

    changeValue(userId,value,flag){
        if(this.state.control === userId || this.state.control == null || !this.state.control || flag){
            const objectSave = {"document":{userId,value}};
            _fire.database().ref("/sample/"+this.props.match.params.id).update(objectSave);
            this.setState({text:value});
        }
    }
    render(){
        const user = this.state.control == this.state.userId || this.state.control ==null ? "You are typing" : " User " + this.state.control + " is typing"
        return(
            <div>
                <div>
                    <h2>Start typing and share link with your friends</h2>
                </div>
                <div style={{width:"80%",marginLeft:"10%"}} >
                    <ReactQuill  id="document" onChange={(event)=>this.changeValue(this.state.userId,event)} onBlur={(event)=>this.changeValue(null,this.state.text,true)}
                        onFocus={(event)=>this.changeValue(this.state.userId,this.state.text)}
                    value={this.state.text} readOnly={this.state.readOnly}/>
                </div>
                <div id="text">
                    {user}
                </div>
            </div>
        )
    }
}