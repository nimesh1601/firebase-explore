import React, { Component } from 'react';
import logo from './logo.svg';
import './App.css';
import {HomeRoute} from './pages/home-root';
import {BrowserRouter} from 'react-router-dom';
class App extends Component {

  guid() {
    function s4() {
      return Math.floor((1 + Math.random()) * 0x10000)
        .toString(16)
        .substring(1);
    }
    return s4() + s4() + '-' + s4() + '-' + s4() + '-' + s4() + '-' + s4() + s4() + s4();
  }

  componentDidMount(){
    if(!localStorage.getItem("id")){
      localStorage.setItem("id",this.guid());
    }
  }

  render() {
    return (
      <div className="App">
        <BrowserRouter>
          {HomeRoute}
        </BrowserRouter>
        
      </div>
    );
  }
}


export default App;
